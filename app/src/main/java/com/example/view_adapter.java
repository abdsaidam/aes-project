package com.example;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

public class view_adapter extends ViewPager {
    public view_adapter(@NonNull Context context) {
        super(context);
        init();
    }

    public view_adapter(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public void  init(){
        setPageTransformer(true,new verticalpage());
        setOverScrollMode(OVER_SCROLL_NEVER);
    }
    private MotionEvent getxy (MotionEvent ev){
        float widdth=getWidth();
        float hieght=getHeight();
        float newx=(ev.getY()/hieght)*widdth ;
        float newy=(ev.getX()/widdth)*hieght ;
        ev.setLocation(newx,newy);
        return ev;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean intercepted =super.onInterceptTouchEvent(getxy(ev));
        getxy(ev);
        return  intercepted;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(getxy(ev));
    }
    private class verticalpage implements ViewPager.PageTransformer{

        @Override
        public void transformPage(@NonNull View page, float position) {
            if (position<-1){
                page.setAlpha(0);
            }else if (position>=1){
                page.setAlpha(1);
                page.setTranslationX(page.getWidth()*-position);
                float yposition =position*page.getHeight();
                page.setTranslationY(yposition);
            }
            else {
                page.setAlpha(0);
            }


        }
    }
}
